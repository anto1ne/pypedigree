#### Description

[Description du problème]

#### Étapes pour le reproduire

1. [Première Étape]
2. [Deuxième Étape]
3. [et ainsi de suite...]

#### Comportement attendu:

[Description du résultat attendu]

#### Comportement actuel:

[Description du comportement actuel]

#### Reproductibilité:

[À quelle fréquence cela se reproduit ?]

#### Versions

[Quelle version du logiciel êtes-vous en train d'utiliser, et les versions de tous les composants et applications pertinentes, en incluant votre système d'exploitation]

#### Information Complémentaires

[Mentionner toutes informations complémentaires, configuration ou données qui peuvent être nécessaire pour reproduire le problème.]

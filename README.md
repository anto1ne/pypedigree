# pyPedigree

pyPedigree est un logiciel dédié à la réalisation d'études de pedigree et de calcul de taux de consanguinité.

## Fonctionnalités

Les fonctionnalités du logiciel sont les suivantes :  
  * ajout/modification/suppression d'individus dans une base de données  
  * affichage du pedigree de chaque individu  
  * simulation de croisement  
  * détermination du taux de consanguinité  
  * génération d'une matrice de parenté pour connaitre la consanguinité des accouplements possibles  
  * listing des individus à l'élévage/à la reproduction/connus  

Un fichier démo est disponible, il se nomme `Animals.xls` et est placé dans le dossier `exemple`.

![pedigree](misc/pedigree.png)

![matrice](misc/matrice.png)

![individus](misc/individus.png)


## Mise en route

Avant de lancer le logiciel pour la première fois, il est recommandé de s'assurer que les outils nécessaires à son fonctionnement soient disponibles. Il est donc recommandé d'installer les packages suivant :

    sudo apt install python3-pyqt5 python3-tabulate python3-xlwt python3-xlrd

Il faut par la suite télécharger pyPedigree, extraire l'archive. Dans le dossier décompressé, exécuter la commande suivante et le logiciel devrait se lancer.

    python pyPedigree.py

## Contribuer/Signaler un bug

Pour contribuer ou signaler un bug, n'hésiter pas à engager une discussion en passant par [un ticket](https://framagit.org/anto1ne/pypedigree/-/issues).
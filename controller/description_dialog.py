#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 18:35:28 2021

@author: antoine
"""

import sys
from PyQt5 import uic, QtCore
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QCompleter

from model.modules import Animal


class DescriptionDialog(QDialog):
    def __init__(self, dtb, animal=None):
        QWidget.__init__(self)
        uic.loadUi('./view/description.ui', self)  # Load the .ui file

        self.dtb = dtb

        # init tab Croisement
        animals = self.dtb.get_animals_all()
        self.males = [an for an in animals if an.sex.upper() == "M"]
        self.females = [an for an in animals if an.sex.upper() == "F"]

        self.cbPere.setEditable(True)
        self.cbPere.clear()
        animals_name = [an.get_name() for an in self.males]
        self.cbPere.addItems(animals_name)
        completer = QCompleter(animals_name)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.cbPere.setCompleter(completer)

        self.cbMere.setEditable(True)
        self.cbMere.clear()
        animals_name = [an.get_name() for an in self.females]
        self.cbMere.addItems(animals_name)
        completer = QCompleter(animals_name)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.cbMere.setCompleter(completer)

        inconnu = Animal("==Non défini==", "", 0, 0, "-", 2021, 0, 0, self.dtb)
        self.cbPere.insertItem(0, inconnu.get_name())
        self.males.insert(0, inconnu)
        self.cbPere.setCurrentIndex(0)
        self.cbMere.insertItem(0, inconnu.get_name())
        self.females.insert(0, inconnu)
        self.cbMere.setCurrentIndex(0)

        self.dict_sex = {}
        self.dict_sex["Male"] = "M"
        self.dict_sex["Femelle"] = "F"
        self.dict_sex["Castré"] = "C"

        self.cbSexe.clear()
        for k, v in self.dict_sex.items():
            self.cbSexe.addItem(k)

        if animal is not None:
            self.set_animal(animal)
        else:
            nb = dtb.get_nb_animals()
            animal_id = "IND" + str(nb)
            while animal_id in self.dtb.animals.keys():
                nb += 1
                animal_id = "IND" + str(nb)
            self.editid.setText(animal_id)

        self.editid.textEdited.connect(self.editid_changed)

    def editid_changed(self):
        animal_id = self.editid.text()
        if animal_id in self.dtb.animals.keys():
            self.editid.setStyleSheet("QLineEdit"
                                      "{"
                                      "background : red;"
                                      "}")
        else:
            self.editid.setStyleSheet("QLineEdit"
                                      "{"
                                      "background : white;"
                                      "}")

    def set_animal(self, animal):
        nom = animal.get_name()
        animal_id = animal.animal_id
        pere = animal.get_sire()
        mere = animal.get_dam()
        sexe = animal.get_sex()
        year = animal.get_year()
        inFarm = animal.inFarm
        toBreed = animal.toBreed

        self.editname.setText(nom)
        self.editid.setText(animal_id)
        self.editid.setEnabled(False)

        self.cbPere.insertItem(0, pere.get_name())
        self.males.insert(0, pere)
        self.cbPere.setCurrentIndex(0)

        self.cbMere.insertItem(0, mere.get_name())
        self.females.insert(0, mere)
        self.cbMere.setCurrentIndex(0)

        for i in range(self.cbSexe.count()):
            if self.dict_sex[self.cbSexe.itemText(i)] == sexe:
                self.cbSexe.setCurrentIndex(i)

        self.dateYear.setValue(int(year))

        self.checkBox_inFarm.setChecked(inFarm)
        self.checkBox_toBreed.setChecked(toBreed)

    def get_infos(self):
        nom = self.editname.text()
        animal_id = self.editid.text()
        pere = self.males[self.cbPere.currentIndex()]
        mere = self.females[self.cbMere.currentIndex()]
        sexe = self.dict_sex[self.cbSexe.currentText()]
        year = self.dateYear.value()
        inFarm = self.checkBox_inFarm.isChecked()
        toBreed = self.checkBox_toBreed.isChecked()
        return Animal(
            nom,
            animal_id,
            pere.animal_id,
            mere.animal_id,
            sexe,
            year,
            inFarm,
            toBreed,
            self.dtb)


if __name__ == '__main__':
    from modules import Dtb
    dtb = Dtb("Animals.xls")

    app = QApplication(sys.argv)
    widget = DescriptionDialog(dtb)
    if widget.exec() == QDialog.Accepted:
        print(widget.get_infos())
        pass

    animal = dtb.get_animals_all()[2]
    widget = DescriptionDialog(dtb, animal)
    if widget.exec() == QDialog.Accepted:
        print(widget.get_infos())
        an = widget.get_infos()
        pass

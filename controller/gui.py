#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 18:55:44 2021

@author: antoine
"""

import sys
import os
from PyQt5 import uic, QtCore
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog, QMessageBox, QStyle

from controller.mainwidget import Mainwidget

__version__ = 0.1


class App(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        uic.loadUi('./view/gui.ui', self)  # Load the .ui file
        self.showMaximized()
        self.actionEnregistrer.setEnabled(False)
        self.actionFermer.setEnabled(False)

        # menu
        self.action_Quitter.triggered.connect(self.quitter_clicked)
        self.action_Ouvrir.triggered.connect(self.ouvrir_clicked)
        self.actionEnregistrer.triggered.connect(self.enregistrer_clicked)
        self.action_Nouveau.triggered.connect(self.nouveau_clicked)
        self.actionFermer.triggered.connect(self.fermer_clicked)

        # Help Menu
        self.action_About_pyPedigree.triggered.connect(self.about_call)
        self.action_Report_a_bug.triggered.connect(self.bug_report_link_call)
        self.action_Website.triggered.connect(self.website_link_call)
        self.action_About_Qt.triggered.connect(self.about_qt_call)

        # icon
        self.action_About_Qt.setIcon(self.style().standardIcon(
            QStyle.SP_TitleBarMenuButton))

        # central widget
        self.widget = Mainwidget(self)
        self.setCentralWidget(self.widget)

    def closeEvent(self, event):
        self.save_before_exit()
        event.accept()

    def quitter_clicked(self):
        self.save_before_exit()
        self.close()

    def fermer_clicked(self):
        self.save_before_exit()
        self.widget.setEnabled(False)
        self.actionFermer.setEnabled(False)

    def save_before_exit(self):
        if (self.widget.dtb is not None) and self.actionEnregistrer.isEnabled():
            answer = QMessageBox.question(
                self,
                "Enregistrer avant de quitter ?",
                "Voulez-vous enregistrer avant de quitter ?",
                QMessageBox.Yes | QMessageBox.No,
                QMessageBox.No)
            if answer == QMessageBox.Yes:
                self.enregistrer_clicked()
                self.widget.setEnabled(False)
                self.actionFermer.setEnabled(False)
                self.dtb = None

    def nouveau_clicked(self):
        self.save_before_exit()

        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        file_name, _file_type = QFileDialog.getSaveFileName(
            self,
            "Entrez le nom du nouveau fichier.",
            "",
            "Fichier xls (*.xls);;All Files (*)",
            options=options)
        if file_name:
            name, extension = os.path.splitext(file_name)
            if extension == ".xls":
                filename = file_name

            else:
                filename = file_name + ".xls"
            self.widget.chargement_fichier(filename)

    def enregistrer_clicked(self):
        self.actionEnregistrer.setEnabled(False)
        self.widget.dtb.save_xls()

    def ouvrir_clicked(self):
        "to open a file"
        self.save_before_exit()

        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        file_name, _file_type = QFileDialog.getOpenFileName(
            self,
            "Sélectionner le fichier",
            "",
            "Fichier xls (*.xls);;All Files (*)",
            options=options)
        if file_name:
            try:
                self.widget.chargement_fichier(file_name)
                self.actionFermer.setEnabled(True)
            except BaseException:
                QMessageBox.warning(
                    self,
                    "Ouverture de fichier",
                    "Erreur à l'ouverture du fichier. Vérifier le respect du format du fichier.")

    # Help Menu
    def about_call(self):
        "about dialog"
        LICENSE_PATH = "./LICENSE"
        licence_file = open(LICENSE_PATH, "r")
        licence = licence_file.read()
        licence_file.close()

        msg = QMessageBox(self)
        msg.setIcon(QMessageBox.Information)

        msg.setText(
            """pyPedigree (version {}) est un logiciel qui permet d'étudier et analyser le pedigree d'animaux.
                    """.format(__version__))
        msg.setInformativeText("Ce logiciel est sous licence libre GPLv3.")
        msg.setWindowTitle("A propos de pyPedigree")
        msg.setDetailedText(licence)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def bug_report_link_call(self):
        "open browser to report bug"
        url = QtCore.QUrl('https://framagit.org/anto1ne/pypedigree/-/issues')
        if not QDesktopServices.openUrl(url):
            QMessageBox.warning(
                self,
                "Ouverture de l'url",
                "Erreur à l'ouverture de l'url.")
        pass

    def website_link_call(self):
        "open browser to report bug"
        url = QtCore.QUrl('https://framagit.org/anto1ne/pypedigree')
        if not QDesktopServices.openUrl(url):
            QMessageBox.warning(
                self,
                "Ouverture de l'url",
                "Erreur à l'ouverture de l'url.")
        pass

    def about_qt_call(self):
        "about qt dialog"
        QMessageBox.aboutQt(self, "A propose de Qt")


if __name__ == '__main__':

    app = QApplication(sys.argv)
    widget = App()
    widget.show()
    sys.exit(app.exec_())

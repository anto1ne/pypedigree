#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 18:15:29 2021

@author: antoine
"""

import sys
from PyQt5 import uic
from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QApplication, QMessageBox, QDialog, QCompleter
from model.modules import Dtb, Animal, print_animals_table
from controller.select_individu_dialog import SelectIndividuDialog
from controller.description_dialog import DescriptionDialog


class Mainwidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        uic.loadUi('./view/mainwidget.ui', self)  # Load the .ui file
        self.parent = parent

        self.setEnabled(False)

        self.dtb = None

        # bouton
        self.boutonAjout.clicked.connect(self.ajout_individu)
        self.boutonEdit.clicked.connect(self.edit_individu)
        self.boutonSuppr.clicked.connect(self.suppr_individu)
        self.boutonStat.clicked.connect(self.stat_dialog)

        # combobox
        self.cbPedigree.currentIndexChanged.connect(self.cbPedigreeChanged)
        self.cbPedigree.setEditable(True)
        self.cbPere.currentIndexChanged.connect(self.cbCroisementChanged)
        self.cbPere.setEditable(True)
        self.cbMere.currentIndexChanged.connect(self.cbCroisementChanged)
        self.cbMere.setEditable(True)

        self.cbIndividus.addItems(
            [
                "Liste des individus de l'élevage",
                "Liste des individus à la reproduction",
                "Liste des individus connus"])
        self.cbIndividus.currentIndexChanged.connect(self.cbIndividusChanged)

        # init tab Matrice de parenté
        self.buttonMatrix.clicked.connect(self.buttonMatrixClicked)

    def cbIndividusChanged(self):
        index = self.cbIndividus.currentIndex()
        if index == 0:
            vec_animal = self.dtb.get_animals_inFarm()
        elif index == 1:
            vec_animal = self.dtb.get_animals_toBreed()
        else:
            vec_animal = self.dtb.get_animals_all()

        self.txtIndividus.setHtml(print_animals_table(vec_animal, "html"))

    def setEnabledactionEnregistrer(self, val):
        if self.parent is not None:
            self.parent.actionEnregistrer.setEnabled(val)

    def stat_dialog(self):
        a = self.dtb.get_nb_animals()
        b = len(self.dtb.get_animals_inFarm())
        c = len(self.dtb.get_animals_toBreed())
        txt = """
        Il y a {} individus dans la base de données.
         -  {} sont présents dans l'élevage.
         -  {} sont disponibles à la reproduction.
        """.format(a, b, c)
        QMessageBox.information(self,
                                "Statistiques",
                                txt)

    def edit_individu(self):
        dialog = SelectIndividuDialog(
            self.dtb.get_animals_all(),
            "Quel est l'individu à modifier ?")
        if dialog.exec() == QDialog.Accepted:
            individu = dialog.get_infos()
            print("Choix de modifier : ", individu)

            dialog = DescriptionDialog(self.dtb, individu)
            if dialog.exec() == QDialog.Accepted:
                individu = dialog.get_infos()
                self.dtb.modif_animal(individu)
                self.update_ui()
                self.setEnabledactionEnregistrer(True)

    def ajout_individu(self):
        dialog = DescriptionDialog(self.dtb)
        if dialog.exec() == QDialog.Accepted:
            individu = dialog.get_infos()
            try:
                self.dtb.ajout_animal(individu)
                self.update_ui()
                self.setEnabledactionEnregistrer(True)
            except BaseException:
                QMessageBox.warning(
                    self,
                    "Ajout d'individu",
                    "Erreur lors de l'ajout de l'individu. Vérifier vos données d'entrées.")

    def suppr_individu(self):
        dialog = SelectIndividuDialog(
            self.dtb.get_animals_all(),
            "Quel est l'individu à supprimer ?")
        if dialog.exec() == QDialog.Accepted:
            individu = dialog.get_infos()
            print("Choix de supprimer : ", individu)
            buttonReply = QMessageBox.question(
                self,
                "Suppression d'individu",
                "Êtes vous sur de vouloir supprimer {} ?".format(individu),
                QMessageBox.Yes | QMessageBox.No,
                QMessageBox.No)
            if buttonReply == QMessageBox.Yes:
                print("Suppression confirmée")
                self.dtb.suppr_animal(individu)
                # mise à jour des combobox
                self.update_ui()
                self.setEnabledactionEnregistrer(True)

    def chargement_fichier(self, filename):
        # ouverture du fichier
        self.dtb = Dtb(filename)
        self.setEnabled(True)
        self.update_ui()

    def update_ui(self):
        # nettoyage avant chargement nouveau fichier
        if self.dtb is not None:
            self.txtMatrix.setPlainText("")
            self.txtIndividus.setPlainText("")

        # init tab Pedigree
        self.animals = self.dtb.get_animals_inFarm()
        animals_name = [an.get_name() for an in self.animals]
        self.cbPedigree.clear()
        self.cbPedigree.addItems(animals_name)
        completer = QCompleter(animals_name)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.cbPedigree.setCompleter(completer)

        # init tab Croisement
        animals = self.dtb.get_animals_toBreed()
        self.males = [an for an in animals if an.sex.upper() == "M"]
        self.females = [an for an in animals if an.sex.upper() == "F"]

        self.cbPere.clear()
        animals_name = [an.get_name() for an in self.males]
        self.cbPere.addItems(animals_name)
        completer = QCompleter(animals_name)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.cbPere.setCompleter(completer)

        self.cbMere.clear()
        animals_name = [an.get_name() for an in self.females]
        self.cbMere.addItems(animals_name)
        completer = QCompleter(animals_name)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.cbMere.setCompleter(completer)

    def buttonMatrixClicked(self):
        self.txtMatrix.setHtml(self.dtb.get_cross_matrix("html"))
        # self.txtMatrix.setPlainText(self.dtb.get_cross_matrix())

    def cbPedigreeChanged(self, index):
        animal = self.animals[index]

        txt = animal.render("html")
        # self.txtPedigree.setPlainText(txt)
        self.txtPedigree.setHtml(txt)

    def cbCroisementChanged(self):
        index_p = self.cbPere.currentIndex()
        index_m = self.cbMere.currentIndex()
        try:
            pere = self.males[index_p]
            mere = self.females[index_m]
            animal_simu = Animal(
                "Simulation de croisement",
                0,
                pere.animal_id,
                mere.animal_id,
                "-",
                2021,
                0,
                0,
                self.dtb)

            txt = animal_simu.render("html")
        except BaseException:
            txt = "Une erreur s'est produite"
        # self.txtCroisement.setPlainText(txt)
        self.txtCroisement.setHtml(txt)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    widget = Mainwidget()
    widget.chargement_fichier("Animals.xls")
    widget.show()
    sys.exit(app.exec_())

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11 18:35:28 2021

@author: antoine
"""

import sys
from PyQt5 import uic
from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QCompleter


class SelectIndividuDialog(QDialog):
    def __init__(self, liste, txt="Texte"):
        QWidget.__init__(self)
        uic.loadUi(
            './view/select_individu_dialog.ui',
            self)  # Load the .ui file

        self.liste = liste
        self.label.setText(txt)
        self.comboBox.setEditable(True)

        animals_name = [an.get_name() for an in liste]
        self.comboBox.addItems(animals_name)
        completer = QCompleter(animals_name)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.comboBox.setCompleter(completer)

    def get_infos(self):
        return self.liste[self.comboBox.currentIndex()]


if __name__ == '__main__':
    from modules import Dtb
    dtb = Dtb("Animals.xls")

    app = QApplication(sys.argv)
    widget = SelectIndividuDialog(dtb.get_animals_toBreed(), "Faites un choix")
    if widget.exec() == QDialog.Accepted:
        print(widget.get_infos())

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../view/description.ui" line="14"/>
        <source>Description de l&apos;individu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/description.ui" line="26"/>
        <source>Nom :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/description.ui" line="36"/>
        <source>Identifiant :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/description.ui" line="66"/>
        <source>Sexe :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/description.ui" line="46"/>
        <source>Père :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/description.ui" line="56"/>
        <source>Mère :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/description.ui" line="76"/>
        <source>Année :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/description.ui" line="96"/>
        <source>Présent dans l&apos;élevage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/description.ui" line="116"/>
        <source>Utilisé pour la reproduction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/select_individu_dialog.ui" line="14"/>
        <source>Veuillez répondre à la question.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/select_individu_dialog.ui" line="33"/>
        <source>Texte à afficher</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../view/gui.ui" line="14"/>
        <source>pyPedigree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="34"/>
        <source>&amp;Fichier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="44"/>
        <source>&amp;Aide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="57"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="78"/>
        <source>&amp;Quitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="81"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="90"/>
        <source>&amp;Ouvrir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="93"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="101"/>
        <source>Enregi&amp;strer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="104"/>
        <source>Enregistrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="107"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="115"/>
        <source>&amp;Nouveau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="118"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="126"/>
        <source>F&amp;ermer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="129"/>
        <source>Ctrl+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="137"/>
        <source>Signale&amp;r un bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="140"/>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="148"/>
        <source>&amp;A propos de pyPedigree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="151"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="156"/>
        <source>A propos &amp;de Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="159"/>
        <source>Ctrl+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="167"/>
        <source>Site &amp;Web</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/gui.ui" line="170"/>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../view/mainwidget.ui" line="14"/>
        <source>pyPedigree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="28"/>
        <source>Ajouter un individu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="39"/>
        <source>Modifier un individu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="50"/>
        <source>Supprimer un individu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="74"/>
        <source>Statistiques</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="98"/>
        <source>Pedigree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="108"/>
        <source>Nom :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="167"/>
        <source>Simuler un croisement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="294"/>
        <source>Calculer puis afficher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="312"/>
        <source>Individus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="373"/>
        <source>&amp;Quitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="376"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="385"/>
        <source>&amp;Ouvrir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="388"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="149"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans Mono&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sélectionner l&apos;animal à afficher.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="177"/>
        <source>Père :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="204"/>
        <source>Mère :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="244"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans Mono&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sélectionner les animaux à croiser.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="262"/>
        <source>Matrice de parenté</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="281"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans Mono&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;La matrice de parenté permet de connaitre rapidement le taux de consanguinité de tous les croisements possibles.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Le temps de calcul peut être long.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/mainwidget.ui" line="350"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choississez la liste des individus à afficher.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

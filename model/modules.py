#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  5 14:38:03 2021

@author: antoine
"""

# https://pythonguides.com/python-read-excel-file/
import xlrd
import xlwt
import copy
import os.path
from tabulate import tabulate

# =============================================================================

STYLE = """
        <style type="text/css">
        table {vertical-align: middle;margin: 0 auto;font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
        table th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}
        table tr {background-color:#ffffff;}
        table td {font-size:12px;border-width: 1px;padding: 0px;border-style: solid;border-color: #729ea5;}
        </style>
        """

# =============================================================================


def indice_debut_gen(m):
    if m == 0:
        val = 0
    else:
        val = indice_debut_gen(m - 1) + 2**m
    return val


def get_position_html(m, l, saut):
    if m == 5:
        return indice_debut_gen(m - 1) + int(l / saut) - 1 + 1
    else:
        return indice_debut_gen(m - 1) + int(l / saut) + 1


def get_parents(vec_animals):
    parents = []
    for asc in vec_animals:
        parents.append(asc.get_sire())
        parents.append(asc.get_dam())
    return parents


def get_generation(position):
    if position == 0:
        return int(1)
    elif 1 <= position < 3:
        return int(2)
    elif 3 <= position < 7:
        return int(3)
    elif 7 <= position < 7 + 8:
        return int(4)
    elif 15 <= position < 15 + 16:
        return int(5)
    elif 31 <= position < 31 + 32:
        return int(6)
    else:
        return None


def get_positions(ca, asc):
    pos = []
    for counter, a in enumerate(asc):
        if a == ca:
            val = get_generation(counter)
            #print(a.get_name(), ca.get_name(), a==ca, val, type(val))
            pos.append(val)  # bug ?
    #print(ca, pos)
    return pos

# =============================================================================


class Animal():
    def __init__(
            self,
            name,
            animal_id,
            sire_id,
            dam_id,
            sex,
            year,
            inFarm,
            toBreed,
            dtb):
        self.name = name
        self.animal_id = animal_id
        self.sire_id = sire_id
        self.dam_id = dam_id
        self.sex = sex
        self.year = year
        self.inFarm = bool(inFarm)
        self.toBreed = bool(toBreed)
        self.dtb = dtb

    def __eq__(self, other):
        return (self.animal_id == other.animal_id)

    def __hash__(self):
        value = hash(self.animal_id)
        return value

    def get_sire(self):
        try:
            return self.dtb.get_animals()[self.sire_id]
        except BaseException:
            return Animal("Inconnu", 0, 0, 0, 0, 0, 0, 0, self.dtb)

    def get_dam(self):
        try:
            return self.dtb.get_animals()[self.dam_id]
        except BaseException:
            return Animal("Inconnu", 0, 0, 0, 0, 0, 0, 0, self.dtb)

    def get_sex(self):
        return str(self.sex)

    def get_year(self):
        return str(self.year)

    def get_name(self):
        if self.name == "":
            return self.animal_id
        else:
            return self.name

    def __repr__(self):
        return self.get_name() + " (" + self.get_sex() + ", " + self.get_year() + ")"

    def get_ascendants(self, gen=5):
        ascendants = [self]
        l_old = [self]
        for g in range(1, gen + 1):
            l_new = get_parents(l_old)
            ascendants.extend(l_new)
            l_old = l_new
        return ascendants

    def get_ascendants_connus(self):
        return [asc for asc in self.get_ascendants() if asc.animal_id != 0]

    def get_pourcentage_ascendants_connus(self):
        nb = len(self.get_ascendants()) - 1  # on retire lui-même
        nb_connus = len(self.get_ascendants_connus()) - 1  # on retire lui-même
        return nb_connus / nb * 100

    def render(self, fmt="txt"):
        ped = self.pedigree(fmt)

        coi = str(round(self.get_COI(), 2))
        inbreed, ignore1, ignore2 = self.get_inbreed()

        if fmt == "html":
            titre = "<h1>{}</h1>".format(self)
            txt = titre + ped + "<p>" + "Consanguinité : " + \
                str(coi) + " %" + "<br />"
            for ca, pos in inbreed.items():
                txt += "<br />" + ca.get_name() + " : " + \
                    " x ".join([str(p) for p in pos])
            txt += "</p>"
        else:
            titre = "=== {} ===\n".format(self)
            txt = titre + ped + "\n" + \
                "Consanguinité : " + str(coi) + " %" + "\n"
            for ca, pos in inbreed.items():
                txt += "\n" + ca.get_name() + " : " + \
                    " x ".join([str(p) for p in pos])

        return txt

    def get_COI(self):
        common_ancestor, common_ancestor_sire, common_ancestor_dam = self.get_inbreed()
        COI = 0.0
        for ca, pos in common_ancestor.items():
            for ps in common_ancestor_sire[ca]:
                for pm in common_ancestor_dam[ca]:
                    COI = COI + (1 / 2)**(ps + pm - 1) * 100
        return COI

    def get_inbreed(self):
        sire = self.get_sire()
        dam = self.get_dam()

        sire_asc = sire.get_ascendants(4)
        dam_asc = dam.get_ascendants(4)

        list_common_ancestor = list(set(sire_asc) & set(dam_asc))
        # print(list_common_ancestor)
        # on prend les ancetres commun cote pere
        common_ancestor_sire = {}
        for ca in list_common_ancestor:
            # print(ca)
            common_ancestor_sire[ca] = get_positions(ca, sire_asc)
        common_ancestor_sire_new = copy.deepcopy(common_ancestor_sire)
        # print(common_ancestor_sire)
        # on vire les doublons du à l'ascendance
        for ca, pos in common_ancestor_sire.items():
            #print(ca, pos)
            for p in pos:
                try:
                    common_ancestor_sire_new[ca.get_sire(
                    )] = common_ancestor_sire_new[ca.get_sire()].remove(p + 1)
                except BaseException:
                    pass
                try:
                    common_ancestor_sire_new[ca.get_dam(
                    )] = common_ancestor_sire_new[ca.get_dam()].remove(p + 1)
                except BaseException:
                    pass
        # print(common_ancestor_sire)
        # print(common_ancestor_sire_new)

        # même chose coté mère
        common_ancestor_dam = {}
        for ca in list_common_ancestor:
            # print(ca)
            common_ancestor_dam[ca] = get_positions(ca, dam_asc)
        common_ancestor_dam_new = copy.deepcopy(common_ancestor_dam)
        # print(common_ancestor_sire)
        # on vire les doublons du à l'ascendance
        for ca, pos in common_ancestor_sire.items():
            #print(ca, pos)
            for p in pos:
                try:
                    common_ancestor_dam_new[ca.get_sire(
                    )] = common_ancestor_dam_new[ca.get_sire()].remove(p + 1)
                except BaseException:
                    pass
                try:
                    common_ancestor_dam_new[ca.get_dam(
                    )] = common_ancestor_dam_new[ca.get_dam()].remove(p + 1)
                except BaseException:
                    pass
        # print(common_ancestor_dam)

        # on fusionne/nettoye
    #    return common_ancestor
        common_ancestor = {}
        common_ancestor_sire = {}
        common_ancestor_dam = {}
        for ca in list_common_ancestor:
            try:
                pos_sire = common_ancestor_sire_new[ca]
                pos_dam = common_ancestor_dam_new[ca]
                if len(pos_sire) >= 1 and len(
                        pos_dam) >= 1 and ca.animal_id != 0:
                    common_ancestor[ca] = pos_sire + pos_dam
                    common_ancestor_sire[ca] = pos_sire
                    common_ancestor_dam[ca] = pos_dam
            except BaseException:
                pass

        return common_ancestor, common_ancestor_sire, common_ancestor_dam

    def pedigree(self, pedfmt="txt"):
        sire = self.get_sire()
        dam = self.get_dam()

        # ====================================================================
        gene = ["\n"]

        cppp = sire.get_sire().get_sire().get_name()
        cpp = sire.get_sire().get_name()
        cppm = sire.get_sire().get_dam().get_name()
        cp = sire.get_name()
        cpmp = sire.get_dam().get_sire().get_name()
        cpm = sire.get_dam().get_name()
        cpmm = sire.get_dam().get_dam().get_name()
        # =====

        l = "\t\t {} \n ".format(cppp)
        gene.append(l)
        l = "\t {} \t \n".format(cpp)
        gene.append(l)
        l = "\t\t {} \n ".format(cppm)
        gene.append(l)

        l = "{} \t\t \n ".format(cp)
        gene.append(l)

        l = "\t\t {} \n ".format(cpmp)
        gene.append(l)
        l = "\t {} \t \n".format(cpm)
        gene.append(l)
        l = "\t\t {} \n ".format(cpmm)
        gene.append(l)

        cmpp = dam.get_sire().get_sire().get_name()
        cmp = dam.get_sire().get_name()
        cmpm = dam.get_sire().get_dam().get_name()
        cm = dam.get_name()
        cmmp = dam.get_dam().get_sire().get_name()
        cmm = dam.get_dam().get_name()
        cmmm = dam.get_dam().get_dam().get_name()
        # =====

        l = "\t\t {} \n ".format(cmpp)
        gene.append(l)
        l = "\t {} \t \n".format(cmp)
        gene.append(l)
        l = "\t\t {} \n ".format(cmpm)
        gene.append(l)

        l = "{} \t\t \n ".format(cm)
        gene.append(l)

        l = "\t\t {} \n ".format(cmmp)
        gene.append(l)
        l = "\t {} \t \n".format(cmm)
        gene.append(l)
        l = "\t\t {} \n ".format(cmmm)
        gene.append(l)

        pedigree_txt = "".join(gene)
        # ====================================================================

        ascendants = self.get_ascendants(5)
        common_ancestor, ignore1, ignore2 = self.get_inbreed()

        nb_ligne = 32

        ligne = []
        for l in range(1, nb_ligne + 1 + 2):
            ligne.append("")

        ligne[0] = "<table>"
        for l in range(1, nb_ligne + 1):
            ligne[l] = "<tr>"
            for m in range(1, 5 + 1):
                nb = 2**(m)
                saut = int(nb_ligne / nb)
                if (l - 1) % saut == 0:
                    animal = ascendants[get_position_html(m, l, saut)]
                    if animal in common_ancestor.keys():
                        name = "<b>{}</b>".format(animal)
                    else:
                        name = animal
                    # name=get_position_html(m,l,saut)
                    ligne[l] += '<td rowspan="{}">{}</td>'.format(saut, name)
        #        else:
        #            ligne[l]+="<td>{}</td>".format(saut,l)
            ligne[l] += "</tr>"
        ligne[l + 1] = "</table>"

        pedigree_html = "\n".join(ligne)

        if pedfmt == "html":
            return STYLE + pedigree_html
        else:
            return pedigree_txt

# =============================================================================


class Dtb():
    def __init__(self, location="Animals.xls"):
        self.location = location
        #location = "Animals.xls"
        self.animals = {}
        self.read_xls()

    def new_xls(self):
        ws_name = "Animals"

        wb = xlwt.Workbook()
        ws = wb.add_sheet(ws_name)
        ws.write(0, 0, "name")
        ws.write(0, 1, "id")
        ws.write(0, 2, "sire")
        ws.write(0, 3, "dam")
        ws.write(0, 4, "sex")
        ws.write(0, 5, "year")
        ws.write(0, 6, "inFarm")
        ws.write(0, 7, "toBreed")
        wb.save(self.location)

    def save_xls(self):
        # on vide d'abord le fichier
        ws_name = "Animals"

        wb = xlwt.Workbook()
        sheet = wb.add_sheet(ws_name)
        sheet.write(0, 0, "name")
        sheet.write(0, 1, "id")
        sheet.write(0, 2, "sire")
        sheet.write(0, 3, "dam")
        sheet.write(0, 4, "sex")
        sheet.write(0, 5, "year")
        sheet.write(0, 6, "inFarm")
        sheet.write(0, 7, "toBreed")

#        # on vide d'abord le fichier
#        nb = len(sheet.col_values(0)) - 1
#        for row in range(1, nb + 1):  # on ignore la première ligne
#            sheet.write(row, 0, "")
#            sheet.write(row, 1, "")
#            sheet.write(row, 2, "")
#            sheet.write(row, 3, "")
#            sheet.write(row, 4, "")
#            sheet.write(row, 5, "")
#            sheet.write(row, 6, "")
#            sheet.write(row, 7, "")

        # on le re-remplit

        #nb = self.get_nb_animals()
        row = 1  # on ignore la première ligne =0
        for animal_id, animal in self.get_animals().items():
            sheet.write(row, 0, animal.name)
            sheet.write(row, 1, str(animal.animal_id))
            sheet.write(row, 2, animal.sire_id)
            sheet.write(row, 3, animal.dam_id)
            sheet.write(row, 4, animal.sex)
            sheet.write(row, 5, animal.year)
            sheet.write(row, 6, animal.inFarm)
            sheet.write(row, 7, animal.toBreed)
            row += 1

        wb.save(self.location)

    def read_xls(self):
        ws_name = "Animals"

        if os.path.isfile(self.location):
            #print ("File exist")
            wb = xlrd.open_workbook(self.location)
            sheet = wb.sheet_by_name(ws_name)

            nb = len(sheet.col_values(0)) - 1
            print("Il y a ", nb, " animaux")

            for row in range(1, nb + 1):  # on ignore la première ligne
                name = sheet.cell_value(row, 0)
                animal_id = str(sheet.cell_value(row, 1))
                sire_id = sheet.cell_value(row, 2)
                dam_id = sheet.cell_value(row, 3)
                sex = sheet.cell_value(row, 4)
                try:
                    year = int(sheet.cell_value(row, 5))
                except BaseException:
                    year = 0
                # print(name)
                inFarm = sheet.cell_value(row, 6)
                toBreed = sheet.cell_value(row, 7)
                animal = Animal(
                    name,
                    animal_id,
                    sire_id,
                    dam_id,
                    sex,
                    year,
                    inFarm,
                    toBreed,
                    self)
                self.animals[animal_id] = animal
        else:
            print("Le fichier n'existe pas. Création du fichier.")
            self.new_xls()

    def get_animals(self):
        return self.animals  # dict

    def modif_animal(self, animal):
        self.animals[animal.animal_id] = animal

    def ajout_animal(self, animal):
        if animal.animal_id == "" or animal.animal_id.upper() in self.animals.keys():
            raise Exception(
                "Absence d'identifiant ou identifiant déjà utilisé")
        else:
            self.animals[animal.animal_id] = animal

    def suppr_animal(self, individu_a_supprimer):
        del self.animals[individu_a_supprimer.animal_id]

    def get_nb_animals(self):
        return len(self.get_animals())

    def get_animal(self, animal_id):
        return self.get_animals()[animal_id]

    def get_animals_all(self):
        vec = []
        for k, an in self.get_animals().items():
            vec.append(an)
        return sorted(vec, key=lambda x: x.get_name().lower())

    def get_animals_inFarm(self):
        vec = []
        for k, an in self.get_animals().items():
            if an.inFarm:
                vec.append(an)
        return sorted(vec, key=lambda x: x.get_name().lower())

    def get_animals_toBreed(self):
        vec = []
        for k, an in self.get_animals().items():
            if an.toBreed:
                vec.append(an)
        return sorted(vec, key=lambda x: x.get_name().lower())

    def get_cross_matrix(self, tablefmt="grid"):
        animals = self.get_animals_toBreed()
        males = [an for an in animals if an.sex.upper() == "M"]
        females = [an for an in animals if an.sex.upper() == "F"]

        # old school
#        string="{:<15}".format("Females/Males")
#        for m in males:
#            string += "| " + "{:<15}".format(m.get_name()) + " "
#        string += "| "
#        string +="\n"
#        string +=(len(string)-2)*"="
#        string +="\n"
#        for f in females:
#            string += ""
#            string += "{:<15}".format(f.get_name())
#            for m in males:
#                string += "| " + "{:<15}".format(str(round(Croisement(m,f).get_COI(),2))) + " "
#            string += "| "
#            string +="\n"
#        return string

        headers = ["Females/Males"]
        for m in males:
            headers.append(m.get_name())

        data = []
        for f in females:
            row = []
            row.append(f.get_name())
            for m in males:
                animal_simu = Animal(
                    "Simulation de croisement",
                    0,
                    m.animal_id,
                    f.animal_id,
                    "M",
                    2021,
                    0,
                    0,
                    self)
                row.append(str(round(animal_simu.get_COI(), 2)))
            data.append(row)

        txt = tabulate(data, headers, tablefmt)
        if tablefmt == "html":
            return STYLE + txt
        else:
            return txt

# =============================================================================


def print_animals_table(vec_animals, tablefmt="grid"):
    headers = ["Nom", "Identifiant", "Sexe", "Année", "Père", "Mère"]

    data = []
    for an in vec_animals:
        row = [
            an.get_name(),
            an.animal_id,
            an.get_sex(),
            an.get_year(),
            an.get_sire().get_name(),
            an.get_dam().get_name()]
        data.append(row)

    txt = tabulate(data, headers, tablefmt)
    if tablefmt == "html":
        return STYLE + txt
    else:
        return txt

# =============================================================================


if __name__ == '__main__':
    dtb = Dtb("exemple/Animals.xls")

    for animal_id, animal in dtb.get_animals().items():
        print("Nom : ", animal, "| Animal id : ", animal_id)

    print("")
    animal_id = dtb.get_animal("FR3275812643")
    print(animal_id)
    print(animal_id.render())
    print("")

    print("")
    print("= Coefficient de consanguinité =")
    print("")
    val = 0
    for animal_id, animal in dtb.get_animals().items():
        nb = animal.get_COI()
        if nb >= val and nb != 0:
            print(animal, " : ", round(nb, 2), "%")
            val = nb
    print("")

    print(dtb.get_cross_matrix())

    print(print_animals_table(dtb.get_animals_inFarm()))

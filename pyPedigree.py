#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 18:39:46 2021

@author: antoine
"""
from controller.gui import App
import sys
import os
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTranslator, QLibraryInfo, QLocale


if __name__ == '__main__':

    app = QApplication(sys.argv)

    LOCALE = QLocale.system().name()
    translator_qt = QTranslator()
    translator_qt.load(
        "qt_" + LOCALE,
        QLibraryInfo.location(
            QLibraryInfo.TranslationsPath))

    LOCALE_PATH = "./locale" + os.path.sep + LOCALE + ".qm"
    translator_app = QTranslator()
    translator_app.load(LOCALE_PATH)

    app.installTranslator(translator_qt)
    app.installTranslator(translator_app)

    widget = App()
    widget.show()
    sys.exit(app.exec_())

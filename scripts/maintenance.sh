#!/bin/bash
# Script to clean directories (erase __py__cache, .pyc) and to correct .py with autopep8

cd ..
echo "Cleaning directories"
py3clean ./
echo "Correcting *.py"
autopep8 --in-place --recursive --aggressive --aggressive ./
#yapf3 --in-place --recursive ../studmanager/
 
